-- Reads a file line by line and nicknames all citizens
-- (who don't currently have a nickname), from the lines in that file

-- TODO:
-- + Test things like outpost liasons, raiding parties/other sites
-- + Move file into DFHack file structure for ease of installation (or make CI do it)
-- + Make this script compliant with DFHack documentation/etc guidelines
-- ? Give an option to turn off succession

local helpstr = [====[

nicks-from-file
=================================
This renames active citizens from a newline-separated file of names.
If -file FILEPATH is not set, defaults to "<df install>/nicknames.txt".
Usage::

    -help
        show this help
    -file FILEPATH
        rename citizens with names from given file.
        DFHack won't read \ characters correctly (it just makes them disappear),
          so you'll have to change all of them to / characters.
        Add quotes (" ") if spaces exist in the file path.
    -clear all|alive
        clears nicknames from units.
        `-clear all` will clear nicknames from alive and dead dwarves.
        `-clear alive` will only clear nicknames from alive dwarves.
        defaults to `alive` if only `-clear` is provided.
    -quiet
        stops script from printing info including
        info about each naming operation

]====]

local windows_troubleshoot = [====[
    - DFHack won't read \ characters correctly (it just makes them disappear),
      so you'll have to change all of them to / characters.
      So, "C:\Users\owner\Desktop\nicknames.txt" would have to become
      "C:/Users/owner/Desktop/nicknames.txt" to work.

    - If there are spaces in your path, you'll have to put double quotes
      around the whole path.
]====]

local linux_troubleshoot = [====[
    - If there are spaces in your path, you'll have to put double quotes
      around the whole path. Single quotes won't work.
]====]

local other_troubleshoot = [====[
    This script hasn't been tested yet with your OS (or dfhack couldn't identify it).
    Some of the following *might* help, but no guarantees.
    - DFHack won't read \ characters correctly (it just makes them disappear),
      so you'll have to change all of them to / characters
      (unless actually escaping things like spaces).

    - If there are spaces in your path, you'll have to put double quotes around
      the whole path. Single quotes won't work.
]====]

utils = require('utils')

validArgs = validArgs or utils.invert({
    'help',
    'file',
    'clear',
    'quiet',
    'random'
})

local order_lookup = {}
local random = nil

-- get all new names from a file, returns an empty list/table
-- if the file does not exist or if there are no new names
function getNamesFromFile(file, cur_dwarf_nicks)
    if not fileReadable(file) then return {} end
    -- Run through nickname file and make an indexed list of nicknames there
    local nicks_from_file = {}
    for line in io.lines(file) do
        if line ~= "" then
            nicks_from_file[#nicks_from_file + 1] = line
        end
    end

    -- randomize if they want
    if random then
        randomize(nicks_from_file)
    end

    -- Init a list of nicknames from nicks_from_file with values of 0
    -- Also init a reverse-lookup list for keeping order later
    local highest_num = {}
    order_lookup = {}
    for i, nick in ipairs(nicks_from_file) do
        highest_num[nick] = 0
        order_lookup[nick] = i
    end

    -- Enumerate the times each nickname is used
    -- A -1 indicates an alive dwarf (so nick should not be used)
    -- TODO: maybe use a mapping of number of times used to a list of names?
    -- TODO: is there a more efficient way to do this? N^2 is gross.
    for i, n in ipairs(nicks_from_file) do
        for d, alive in pairs(cur_dwarf_nicks) do
            val = getNumSuffix(d, n)
            if val ~= nil then
                if alive then
                    highest_num[n] = -1
                elseif highest_num[n] > -1 then
                    if val > highest_num[n] then
                        highest_num[n] = val
                    end
                end
            end
        end
    end

    -- Create list with just useable names (use preserveOrder func to keep order)
    local new_names = {}
    for nick,v in spairs(highest_num, preserveOrder) do
        if v > 0 then
            new_names[#new_names+1] = nick..' '..toRoman(v+1)
        elseif v == 0 then
            new_names[#new_names+1] = nick
        end
    end

    return new_names
end

--------------------------------------------------------------------------------
-- Roman Numeral <-> Integer conversion
--------------------------------------------------------------------------------

local RN_M = {"", "M", "MM", "MMM"}
local RN_C = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}
local RN_X = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}
local RN_I = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}

function toRoman(num)
    if num < 1 then
        return ""
    elseif num > 3999 then
        return tostring(num)
    end

    -- NOTE: floor division operator (//) is like integer division in other languages
    -- By default, Lua converts everything to float in normal division (/)
    local t = {RN_M[(num // 1000)+1],
               RN_C[(num % 1000 // 100)+1],
               RN_X[(num % 100 // 10)+1],
               RN_I[(num % 10)+1]
               }

    return table.concat(t,"")
end

function fromRoman(num)
    local roman_numerals = {I=1, V=5, X=10, L=50, C=100, D=500, M=1000}
    local result = 0
    for i=1, #num do
        cur_ss = string.sub(num, i, i)
        next_ss = string.sub(num, i+1, i+1)
        if i == #num or roman_numerals[cur_ss] >= roman_numerals[next_ss] then
            result = result + roman_numerals[cur_ss]
        else
            result = result - roman_numerals[cur_ss]
        end
    end
    return result
end

--------------------------------------------------------------------------------
-- Helper Functions
--------------------------------------------------------------------------------

function fileReadable(filepath)
   local f = io.open(filepath, "r")
   if f ~= nil then io.close(f) return true else return false end
end

function getNumSuffix(dwarf_name, base_nick)
    if dwarf_name == base_nick then
        return 1
    elseif startsWith(dwarf_name, base_nick) then
        local suffix = string.sub(dwarf_name, #base_nick+1) -- should include space
        if startsWith(suffix, ' ') then
            local good, result = pcall(fromRoman, string.sub(suffix, 2)) -- start at 2 to remove space
            if good then
                return result
            else
                local errnn = result:find ( "nil with number" )
                local errtn = result:find ( "two nil values" )
                local erran = result:find ( "arithmetic on a nil value" )
                if errnn ~= nil or errtn ~=nil or erran ~= nil then
                    return nil
                else
                    error(result, 0)
                end
            end
        end
    end
    return nil
end

-- startsWith from http://lua-users.org/wiki/StringRecipes
function startsWith(str, start)
    return string.sub(str, 1, #start) == start
end

function preserveOrder(t,a,b)
    if t[b] == t[a] then
        return order_lookup[b] > order_lookup[a]
    else
        return t[b] > t[a]
    end
end

-- From https://www.programming-idioms.org/idiom/10/shuffle-a-list/2019/lua
function randomize(list)
    math.randomseed(os.time())
    for i = #list, 2, -1 do
        local j = math.random(i)
        list[i], list[j] = list[j], list[i]
    end
end

-- From https://stackoverflow.com/questions/15706270/sort-a-table-in-lua
function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

function isAliveDwarf(unit) -- can be non-citizen? and insane
    local alive = dfhack.units.isAlive(unit)
    local dwarf = dfhack.units.isDwarf(unit)
    return alive and dwarf
end

function printTroubleshooting(filepath)
    local os_type = dfhack.getOSType()
    if os_type == "windows" then
        print(windows_troubleshoot)
    elseif os_type == "linux" then
        print(linux_troubleshoot)
    else
        print(other_troubleshoot)
    end
    print()
    print("    If none of the above helps, double check that the file is\n    accessible at the given path.")
    print()
    print("    DFHack thinks the filepath is:")
    print(string.format("    %s", filepath))
    return
end

--------------------------------------------------------------------------------
-- Start main
--------------------------------------------------------------------------------

if moduleMode then
    return
end

local args = utils.processArgs({...}, validArgs)
local filepath = string.format("%s%snicknames.txt", dfhack.getDFPath(), package.config:sub(1,1))
local file_lines = nil

if args.help then
    print(helpstr)
    return
end

local quiet = args.quiet ~= nil
local clear = args.clear
random = args.random ~= nil

-- Get the filepath and make sure it exists
if clear == nil then
    if args.file then
        if args.file == "" then
            print(helpstr)
            return
        elseif fileReadable(args.file) then
            filepath = args.file
        else
            print("========================================================================")
            print("    ERROR: Couldn't read the given file. See below for possible reasons.")
            print()
            printTroubleshooting(args.file)
            print("========================================================================")
            return
        end
    else
        if not fileReadable(filepath) then
            print("    ERROR: No nickname file found at default location:")
            print(string.format("    \"%s\"", filepath))
            print("Put your file there or give a filepath with the -file option.")
            print("For more help use -help.")
            return
        end
    end
end

-- if clearing names, don't worry about checking for nicknames, just blast them all
if clear ~= nil then
    local clear_alive = clear == "alive" or clear == ""
    local clear_all = clear == "all"
    for i, unit in ipairs(df.global.world.units.active) do
        local clear_me = clear_all or (clear_alive and isAliveDwarf(unit))
        if clear_me then
            if not quiet and unit.name.nickname ~= "" then
                print(string.format("Clearing %s `%s` --> ``",
                                    tostring(unit.name.first_name),
                                    tostring(unit.name.nickname)))
            end
            dfhack.units.setNickname(unit, "")
        end
    end
else
    print(string.format("Reading nicknames from \"%s\"", filepath))
    local cur_dwarf_nicks = {}
    local new_name_list = {}
    -- run through to build list of already-used nicknames and whether they're alive
    for i, unit in ipairs(df.global.world.units.active) do
        if unit.name.nickname ~= "" then
            local key = unit.name.nickname
            if isAliveDwarf(unit) then
                -- is just alive. Might not be sane or a member of the fortress,
                -- but if it has a nickname, we should know about it
                cur_dwarf_nicks[key] = true
            else
                cur_dwarf_nicks[key] = false
            end
        end
    end
    local result = getNamesFromFile(filepath, cur_dwarf_nicks)

    -- now run through active dwarves and nickname unnicknamed ones
    local new_name_i = 1
    for i, unit in ipairs(df.global.world.units.active) do
        if dfhack.units.isCitizen(unit) and unit.name.nickname == "" then  -- unnicknamed dwarf
            nick = result[new_name_i]
            if nick == nil then                        -- EOF
                if not quiet then print("No more new nicknames from the given file") end
                break
            else
                if not quiet then
                    print(string.format("Nicknaming %s --> `%s`",
                                        tostring(unit.name.first_name), nick))
                end
                dfhack.units.setNickname(unit, nick)
                new_name_i = new_name_i + 1
            end
        end
    end
    print("Finished nicknaming dwarves")
end

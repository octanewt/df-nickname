# Dwarf Fortress Mass Nicknaming Tool

Just for kicks and to see if I could, I wrote a DFhack script that renames dwarves from a list in a text file. It's pretty nice to not have to rename them one by one now.

Right now it just takes a text file and renames alive dwarves from that file. So a file that looks like this:

```
Sue
Bob
Jill
Joe
```

Will be read in and nickname 4 alive dwarves.

The default location it will look for a `nicknames.txt` file is in the root folder of the Dwarf Fortress installation.

## Succession! (kinda)

The script will go through all dead nicknamed dwarves and find the highest number Roman numeral suffix, and name the next alive dwarf with that number +1. So, for example:

1. You have a list like the one above, and have nicknamed your dwarves with it.
2. Bob dies (not due to your neglect, I'm sure).
3. You run `nicks-from-file` again, and a previously unnicknamed Urist is now nicknamed "Bob II"

Currently, only dwarves with nicknames of the form `<nick> <NUM>` (e.g. "Bob IV") will count toward the number, so if you manually rename "Bob II" to "Bob Jr." or "Bob 2.0," the script won't pick that up, and will proceed with nicknaming another dwarf "Bob II" next time it is run.

---

## Known issues

DFHack is weird about inputting file paths on the command line, which limits the general usefulness for those who don't know why backslashes all "disappear."

### Windows paths

- DFHack won't read `\` characters correctly (they just disappear),
  so you'll have to change all of them to `/` characters.
  So, `"C:\Users\owner\Desktop\nicknames.txt"` would have to become
  `"C:/Users/owner/Desktop/nicknames.txt"` to work.

- If there are spaces in your path, you'll have to put double quotes
  around the whole path (as shown above.)

### Mac/Linux paths

- If there are spaces in your path, you'll have to put double quotes
  around the whole path. Single quotes won't work.

- I haven't tested on Mac, but it's probably the same as Linux.

---

## Command line options

- `-help` shows the list of command line options with some added help (much like this list)
- `-file FILEPATH` renames citizens with names from provided filepath instead of the default file.
    - DFHack won't read `\` characters correctly (they just disappear), so you'll have to change all of them to `/` characters.
    - Add quotes (`" "`) if spaces exist in the file path.
- `-clear all|alive` clears all nicknames from units (**not just those added by this script!**).
    - `-clear all` will clear nicknames from alive and dead dwarves.
    - `-clear alive` will only clear nicknames from alive dwarves.
    - Defaults to `alive` if only `-clear` is provided.
- `-quiet` stops script from printing info including info about each naming operation.

---

## Installation

1. Copy `nicks-from-file.lua` into `<df root>/hack/scripts`.
2. Copy `nicknames.txt` into the root DF folder or create your own text file named `nicknames.txt` and save it there.
3. Now, in the DFhack terminal, you should be able to run `nicks-from-file`.
4. Profit!
